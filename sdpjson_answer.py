import asyncio
import sdp_transform
import json


class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        message = json.loads(message)  # Para imprimir diccionario de python normal en las siguientes lineas
        sdp_document = {'version': 0,
                        'origin': {'username': 'user',
                                   'sessionId': 434344,
                                   'sessionVersion': 0,
                                   'netType': 'IN',
                                   'ipVer': 4,
                                   'address': '127.0.0.1'},
                        'name': 'Session',
                        'timing': {'start': 0,
                                   'stop': 0},
                        'connection': {'version': 4,
                                       'ip': '127.0.0.1'},
                        'media': [{'rtp': [{'payload': 0, 'codec': 'PCMU', 'rate': 8000},
                                           {'payload': 96, 'codec': 'opus', 'rate': 48000}],
                                   'type': 'audio',
                                   'port': 34543,
                                   'protocol': 'RTP/SAVPF',
                                   'payloads': '0 96',
                                   'ptime': 20,
                                   'direction': 'sendrecv'},
                                  {'rtp': [{'codec': 'H264', 'payload': 97, 'rate': 90000},
                                           {'codec': 'VP8', 'payload': 98, 'rate': 90000}],
                                   'type': 'video',
                                   'port': 34543,
                                   'protocol': 'RTP/SAVPF',
                                   'payloads': '97 98',
                                   'direction': 'sendrecv'}]}

        msg = sdp_transform.write(sdp_document)
        answer = json.dumps({'type': 'answer', 'sdp': msg})


        print('Received %r from %s' % (message, addr))
        print('Send %r to %s' % (answer, addr))
        self.transport.sendto(answer.encode(), addr)


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())