import asyncio

async def main():
    print('¡Hola ...')
    await asyncio.sleep(1)  # Espera 1 segundo. Si espera un segundo,
                            # si hay otras rutinas se van ejecutando mientras tanto.
    print('... mundo!')

# asyncio.run(main())
    # Hasta que la corutina no termine, no hace nada.
main()
    # Funciones asíncronas, que pueden trabajar en paralelo con otras funciones.